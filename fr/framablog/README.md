# Participer au <b class="violet">Frama</b><b class="rouge">blog</b>

Il y a plusieurs manières de participer au <b class="violet">Frama</b><b class="rouge">blog</b>. La première, et non des moindres, c'est de participer à <b class="violet">Frama</b><b class="rouge">lang</b>, le groupe de traduction de nouvelles du monde du libre. Toutes les infos <a href="/framalang/index.html">sur <b class="violet">Frama</b><b class="rouge">lang</b> sont ici</a>.

## Que puis-je rédiger&nbsp;?

Si vous aimez rédiger et vulgariser, vous pouvez donc&hellip;

* &hellip;[Interviewer des initiatives libres](interviews.html)
* &hellip;[Proposer des articles de temps en temps](articles.html#articles-exceptionnels)
* &hellip;[Devenir clavier invité](articles.html#les-claviers-invités)

<div class="well">
<strong>ATTENTION&nbsp;:</strong>
<p>Notez bien que, par défaut, tout contenu publié sur le <b class="violet">Frama</b><b class="rouge">blog</b> l'est sous licence <a href="https://creativecommons.org/licenses/by-sa/3.0/fr/">CC-BY-SA</a>. Si vous désirez proposer vos rédactions sous une autre licence (libre, bien entendu) pensez à le mentionner dans nos échanges&nbsp;!</p>
</div>

## Ligne éditoriale du <b class="violet">Frama</b><b class="rouge">blog</b>

### Le <b class="violet">Frama</b><b class="rouge">blog</b> parle de libre

Nous préférons parler de projets sous licences libres que sous licences de libre diffusion (par exemple&nbsp;: éviter les mentions -NC et -ND chez <i lang="en">Creative Commons</i>). Il nous arrive de faire une entorse à cette règle, mais cela doit se justifier.

### Le <b class="violet">Frama</b><b class="rouge">blog</b> s'adresse aux «&nbsp;moldu·e·s du code&nbsp;»

Nous sommes dans une volonté d'éducation populaire au libre et à sa culture. Il existe de nombreux blogs spécialisés sur les subtilités du développement, de l'administration-système, des distributions, etc. Si nous aimons les lire, nous savons que ce n'est ni ce que vient chercher le lectorat du <b class="violet">Frama</b><b class="rouge">blog</b>, ni ce que nous souhaitons y proposer.

Nous cherchons donc, dans le fond (choix des sujets) comme dans la forme (explications des terminologies, illustrations, etc.) à partager et faire découvrir de belles choses de ce monde aux personnes qui n'en font pas (enore) partie. ;)

### Le <b class="violet">Frama</b><b class="rouge">blog</b> est lu par des plus spécialistes que nous

Et cela  nous honore&nbsp;! En retour, nous souhaitons donc à être rigoureux·ses dans les termes, complet·e·s dans la manière de couvrir un sujet, bref&nbsp;: à ne pas (trop) dire de bêtises&nbsp;! ^^

Le but, ici, est d'entretenir (et d'être à la hauteur de) cette confiance mutuelle qui fait que des personnes spécialisées dans le numérique se servent de ce blog comme d'un outil pour sensibiliser leur entourage.

Donc, on croise ses sources, on les vérifie, et on les partage.

### Le <b class="violet">Frama</b><b class="rouge">blog</b> ne se prend pas au sérieux

Oui&nbsp;: on peut faire les choses sérieusement sans se croire sorti de la cuisse de GNU&nbsp;! Un peu d'auto-dérision sur nos travers, nos sujets trollesques, notre <i lang="en">fandom</i> ne fait pas de mal&hellip; Attention toutefois à ne pas verser systématiquement dans les blagues d'initié·e·s, qui souvent ferment la porte aux personnes n'ayant pas les mêmes références (ces vannes sont la cerise sur le gâteau, pas la crème pâtissière&nbsp;!).

### Le <b class="violet">Frama</b><b class="rouge">blog</b> tente d'être bienveillant

Ce qui va sans dire va souvent mieux en le disant&nbsp;: les intolérances, oppressions, etc. c'est **non** et ce n'est pas négociable&nbsp;: c'est [dans nos CGU](https://framasoft.org/nav/html/cgu.html). Bien entendu, on ne peut être sensibilisé·e à tous les sujets, mais si on nous reporte un <i lang="en">bug</i> à ce niveau-là&nbsp;: il faut apporter un <i lang="en">bugfix</i>&nbsp;! ;)

Au delà de cette question, rares seront les articles visant le <i lang="en">clash</i>, la confrontation, l'opprobre, etc. qui seront acceptés. Nous préférons mettre en valeur le positif et tenter d'éveiller les consciences sur les dangers et enjeux, bref&nbsp;: faire de l'éducation populaire&nbsp;!

Les articles coup de gueule ne sont pas proscrits, mais c'est comme tout&nbsp;: s'il y en a tous les jours ça perd de son impact.

### Le <b class="violet">Frama</b><b class="rouge">blog</b> écrit en français correct

Le blog est pointilleux (voire pire&nbsp;: intransigeant) sur le bon usage de la grammaire, de l'orthographe et de la typographie. Les pénibles de l'espace insécable, c'est nous&nbsp;!

Relisez-vous, et réclamez des relectures.

## Pour nous rejoindre, contactez-nous&nbsp;!

Vous désirez apporter votre plume (ok&nbsp;: votre clavier) et participer à la rédaction du <b class="violet">Frama</b><b class="rouge">blog</b>&nbsp;? Vous avez bien lu les autres pages de ce guide et êtes motivé·e·s&nbsp;?

Chouette&nbsp;! Il vous suffit de [nous contacter](https://contact.framasoft.org/#Framablog) ;)

*Et une fois que c'est fait, n'hésitez pas à voir [comment dompter notre interface blog](mise-en-page.html).*
