# Summary

* [Accueil](README.md)


## Projets communautaires

* [Framablog - articles & ITW](framablog/README.md)
  * [Framablog - interviews](framablog/interviews.md)
  * [Framablog - articles](framablog/articles.md)
  * [Framablog - mise en page](framablog/mise-en-page.md)
* [Framabook - édition libre](framabook/README.md)
* [Framacode - développement](framacode/README.md)
  * [Contribuer facilement sur Framagit (traduction, rédaction)](framacode/git-en-ligne.md)
  * [Contribuer aux documentations](framacode/gitbook.md)
  * [Methode de travail git](framacode/workflow-git.md)
* [Framakey - clé usb](framakey/README.md)
  * [Télécharger le pack de portabilisation](framakey/portabiliser-pack.md)
  * [Portabiliser - Préambule](framakey/portabiliser-tuto-intro.md)
  * [Portabiliser - 1. Préparons le terrain](framakey/portabiliser-tuto-1.md)
  * [Portabiliser - 2. Rappels sur la discrétion et la portabilité](framakey/portabiliser-tuto-2.md)
  * [Portabiliser - 3. Testons la discretion](framakey/portabiliser-tuto-3.md)
  * [Portabiliser - 4. Testons la portabilité](framakey/portabiliser-tuto-4.md)
  * [Portabiliser - 5. Réalisation du lanceur](framakey/portabiliser-tuto-5.md)
  * [Portabiliser - 6. Publication de l’application](framakey/portabiliser-tuto-6.md)
* [Framalang - traduction](framalang/README.md)
  * [Framalang - Bienvenue](framalang/bienvenue.md)
  * [Framalang - Processus](framalang/chemin.md)
  * [Framalang - Outils](framalang/outils.md)
  * [Framalang - Vers le blog](framalang/blog.md)
* [Framalibre](framalibre/README.md)
  * [Premiers pas sur Framalibre](framalibre/premiers-pas.md)
  * [De bonnes raisons pour se créer un compte](framalibre/se-creer-un-compte.md)
  * [Comment créer ou modifier une notice ?](framalibre/creer-modifier-une-notice.md)

## Dégoogliser

* [Flyers, affiches, stickers](print/README.md)
  * [Print - Framasoft](print/framasoft.md)
  * [Print - Dégooglisons](print/degooglisons.md)
  * [Print - Contributopia](print/contributopia.md)
* [Liens essentiels](essentiels/README.md)
* [Conférences, présentations](conferences/README.md)
