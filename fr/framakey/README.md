# Documentation

## Portabiliser un logiciel

 *  [Télécharger le pack de portabilisation](portabiliser-pack.html)
 *  [Préambule](portabiliser-tuto-intro.html)
 *  [Préparons le terrain](portabiliser-tuto-1.html)
 *  [Rappels sur la discrétion et la portabilité](portabiliser-tuto-2.html)
 *  [Testons la discrétion](portabiliser-tuto-3.html)
 *  [Testons la portabilité](portabiliser-tuto-4.html)
 *  [Réalisation du lanceur](portabiliser-tuto-5.html)
 *  [Publication de l’application](portabiliser-tuto-6.html)

## Créer un pack

 *  [Tutoriel de création d'une image personnalisée Framakey - Linux Mint](https://files.framakey.org/testing/main/packages/FKMint/Tutoriel_Image_FK_Mint.pdf)

## Liens externes

 * [Framakey](https://framakey.org)
 * [La foire aux questions Framakey](https://contact.framasoft.org/#framakey)
 * [Le forum de la Framakey](https://framacolibri.org/c/qualite/framakey)
