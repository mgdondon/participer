## Contribute to documentation

### Drafting

Files are written with [markdown syntax](https://docs.framasoft.org/fr/mattermost/help/messaging/formatting-text.html) but gitbook allows HTML syntax too.

However, it is not possible to mix the 2 syntaxes in the same block.

If you want to add internal links, be careful to point to
the `.html` pages corresponding to the `.md` pages of the folder.

The Framaprojet doc home page may contain a
presentation part + quick start + link to source software
and a presentation part of the source software (it can be from the official doc)
table of contents with the list of pages found in the submenu on the left.

### Work method

  1. Fork
  2. Create a branch
  3. Make changes (commits)
  4. Ask for a merge request
  5. Discuss/edit

### Structure

English and French documentation are completely independent.

The documentation of each Frama project must be recorded in a file
corresponding to the name of the source software (example: Framacarte → in the `umap` folder).

In this folder, create an `images` folder containing the screenshots
and a `README.md` file which will contain the project presentation home page

Then add the Framaprojet in the left column and on the home page,
you must edit the `SUMMARY.md` and `README.md` files (at the root in the `en` folder).

In `SUMMARY.md`, it is not possible to display a tree structure
of more than 2 levels and titles are not taken into account
(they are left only as an indication).

Each `.md` link found in this file will be turn into the corresponding `.html` file by Gitbook.
Files not found there will be placed in the folder
as simple static files (like images)

## Create local copy of the documentation

To get a preview of the site rendering it may be useful to install Gitbook locally.

### Gitbook installation

Install `NodeJS`, `NPM` and `gitbook` client:

```
sudo apt-get install nodejs-legacy npm
sudo npm install gitbook-cli -g
```

### generate documentation

Clone your documentation fork

```
git clone https://framagit.org/<user>/framasoft/docs.git
```

Form `docs` folder, proceed to requiered plugins (plugins list and their configuration are in `book.json`)

```
cd docs
gitbook init
gitbook install
gitbook serve --no-watch
```
If everything is good (command ends by `Serving book on…`), local copy can be found at this adress: http://localhost:4000/

The `--no-watch` option prevents each change in the
files causes an automatic restart of the nodejs server
which can be heavy in the long run.
The more pages, the longer the loading time.

It is better to take the time to finish the changes and restart
the server manually when we're done.

If you have a local web server, rather than using the server
nodejs, you can also create the site manually with the command:

```
gitbook build
```
and copy `_book` folder wherever you want on your server.
